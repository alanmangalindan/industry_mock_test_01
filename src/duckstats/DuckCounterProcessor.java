package duckstats;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class imports the duck statistics from a file, prints the first 10 duck
 * statistics, and the first 10 duck statistics in descending order.
 * 
 * @author Yu-Cheng Tu
 *
 */
public class DuckCounterProcessor {

	/**
	 * This is the main method, you may modify this method appropriately.
	 */
	public static void main(String[] args) {
		DuckCounterProcessor dp = new DuckCounterProcessor();

		System.out.println("Processing Duck Data");
		System.out.println("----------------------");
		String currentDir = System.getProperty("user.dir");
		List<DuckCounter> duckData = dp.processFile(currentDir + "/ducks.csv");
		
		System.out.println();
		System.out.println("=================");
		System.out.println("First 10 Duck Stats");
		System.out.println("=================");
		dp.printFirstTenDuckCounts(duckData);
		System.out.println("=================");

		System.out.println("First 10 Duck Stats in Descending Order");
		System.out.println("=================");
		
		// step c.
		Collections.sort(duckData);
		dp.printFirstTenDuckCounts(duckData);
		System.out.println("=================");
	}

	// step b ii.
	private void printFirstTenDuckCounts(List<DuckCounter> duckData) {

		for (int i = 0; i < 10; i++) {
			DuckCounter d = duckData.get(i);
			System.out.println("Date: " + d.getDate().toString() + ", ducks: " + d.getDuckCount() +
					", ducklings: " + d.getDucklingCount());
		}
		
	}

	// step b i.
	private List<DuckCounter> processFile(String filePath) {

//		File duckFile = new File(filePath);

		List<DuckCounter> duckList = new ArrayList<>();

		try (BufferedReader bR = new BufferedReader(new FileReader(filePath))) { // can pass in String filePath directly

			String line;

			while ((line = bR.readLine()) != null) { // reads line form file, assigns to line and checks if line is null
				String[] lineData = line.split(",");
				int ducks = Integer.parseInt(lineData[1]);
				int ducklings = Integer.parseInt(lineData[2]);
				DuckCounter d = new DuckCounter(lineData[0], ducks, ducklings);
				duckList.add(d);
			}

		} catch (IOException e) {
			System.out.println(e.getMessage());
		} catch (DuckCounterException e) {
			System.out.println(e.getMessage());
		}

		return duckList;

	}

}
