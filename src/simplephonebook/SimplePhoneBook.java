package simplephonebook;

import java.util.HashMap;
import java.util.Map;
/**
 * Complete this class for adding names and phone numbers to a collection.
 * @author Write your UPI here.
 *
 */
public class SimplePhoneBook {

	private Map<String, Integer> phoneBook = null;

	public SimplePhoneBook() {
		phoneBook = new HashMap<>();
	}
	
	// 2a. Adds multiple contacts to the phone book collection
	public void addMultipleContacts(Map<String, Integer> multipleContacts) {

		for (String key: multipleContacts.keySet()) {
			phoneBook.put(key, multipleContacts.get(key));
		}
	}
	
	// 2b. Adds a single contact to the phone book collection
	public void addNewContact(String name, int number) {

		if (phoneBook.containsKey(name)) {
			System.out.println("Current number for " + name + " is " + phoneBook.get(name) + ".");
		}

		phoneBook.put(name, number);

		System.out.println("Number for " + name + " is " + phoneBook.get(name) + ".");

	}
	
	// 2c. Prints the number for a given name
	public void printNumberFor(String name) {

		if (phoneBook.containsKey(name)) {
			System.out.println("Current number for " + name + " is " + phoneBook.get(name) + ".");
		} else {
			System.out.println("The person " + name + " does not exist in the phone book.");
		}

	}
	
	// 2d. Prints the total number of entries of the phone book collection
	public void printTotalNumberOfEntries(){

		System.out.println("There are " + phoneBook.size() + " entries in the phone book.");

	}
	
	// 2e. Prints the name for a given phone number
	public void printNameByPhoneNumber(int phoneNumber) {

		for (String key: phoneBook.keySet()) {
			if (phoneBook.get(key) == phoneNumber) {
				System.out.println("The person with the number " + phoneNumber + " is: " + key + ".");
				return;
			}
		}

		// alternate way to get matching name for given phoneNumber
//		for (Map.Entry<String, Integer> entry: phoneBook.entrySet()) {
//			if (entry.getValue() == phoneNumber) {
//				System.out.println("The person with the number " + phoneNumber + " is: " + entry.getKey() + ".");
//				return;
//			}
//		}

		System.out.println("Cannot find person in the phone book with the number " + phoneNumber + ".");

	}
}
