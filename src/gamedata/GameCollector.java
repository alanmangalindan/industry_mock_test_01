package gamedata;


import java.io.*;
import java.util.*;

/**
 * This class imports the games.csv file, and prints games from the collection.
 * 
 * @author Yu-Cheng Tu
 *
 */
public class GameCollector {
	List<Game> games;

	/**
	 * This is the main method, do not modify this.
	 */
	public static void main(String[] args) {
		GameCollector gc = new GameCollector();

		String currentDir = System.getProperty("user.dir");
		gc.processFile(currentDir + "/games.csv");

		System.out.println("The first 5 games");
		System.out.println("===================");
		gc.printFirstFiveGames();
		System.out.println("===================");

		System.out.println("Number of Action games in the collection: " + gc.getNumberOfGamesByGenre(Genre.ACTION));
		System.out.println("Number of Puzzle games in the collection: " + gc.getNumberOfGamesByGenre(Genre.PUZZLE));
		System.out.println("Number of Role-Playing games in the collection: " + gc.getNumberOfGamesByGenre(Genre.RPG));
		System.out.println("Number of Sports games in the collection: " + gc.getNumberOfGamesByGenre(Genre.SPORTS));
		System.out.println("===================");

		System.out.println("The top 10 games by year and publisher");
		System.out.println("===================");
		gc.printTopTenSortedGames();
		System.out.println("===================");

		gc.exportSortedGames(currentDir + "/sortedGames.csv");

		System.out.println("Number of games per year");
		System.out.println("===================");
		Map<Integer, Integer> numOfGamesPerYear = gc.getCountOfGamesPerYear();
		gc.printGamesPerYear(numOfGamesPerYear);
	}

	private void printGamesPerYear(Map<Integer, Integer> numOfGamesPerYear) {
		Set<Integer> years = numOfGamesPerYear.keySet();
		for (Integer year : years) {
			System.out.printf("Games:%-5d Year: %4d%n", numOfGamesPerYear.get(year), year);
		}

	}

	// step c i.
	private int checkYear(String yearStr) throws InvalidYearException, GameInFutureException {

		int year;

		if (yearStr.isEmpty()) { // or yearStr.equals("")
			return 2016;
		}

		year = Integer.parseInt(yearStr);
		if (year < 1950) {
			throw new InvalidYearException("The game is too old!");
		} else if (year > 2016) {
			throw new GameInFutureException("The game is too new!");
		}

		return year;
	}

	// step c ii.
	private Genre getGenre(String genre) {

		switch (genre.toLowerCase()) {
			case "action": return Genre.ACTION;
			case "misc": return Genre.MISC;
			case "role-playing": return Genre.RPG;
			case "puzzle": return Genre.PUZZLE;
			case "sports": return Genre.SPORTS;
			default: return Genre.OTHER;
		}
	}

	// step c iii.
	private Game processGameDetails(String[] gameDetails) throws InvalidYearException, GameInFutureException {
		int id = Integer.parseInt(gameDetails[0]);
		String game = gameDetails[1];
		String platform = gameDetails[2];
		int year = checkYear(gameDetails[3]);
		Genre genre = getGenre(gameDetails[4]);
		String publisher = gameDetails[5];
		return new Game(id, game, platform, year, genre, publisher);
	}

	// step c iv.
	private void processFile(String filePath) {

		File myFile = new File(filePath);

		games = new ArrayList<>();

		try (BufferedReader bR = new BufferedReader(new FileReader(myFile))) {

			String line = bR.readLine(); // disregard first line which just the header line

			while ((line = bR.readLine()) != null) {
				String[] lineData = line.split(",");
				try {
					Game thisGame = processGameDetails(lineData);
					games.add(thisGame);
				} catch (InvalidYearException e) {
					System.out.println(e.getMessage());
				} catch (GameInFutureException e) {
					System.out.println(e.getMessage());
				} catch (NumberFormatException e) {			// not needed as it was not explicitly in the instructions
					System.out.println(e.getMessage());
				}
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
	}

	// step c v.
	private void printFirstFiveGames() {

		for (int i = 0; i < 5; i++) {
			System.out.println(games.get(i).toString());
		}

	}

	// step c vi.
	private int getNumberOfGamesByGenre(Genre genre) {

		int count = 0;
//		String genreToCount = genre.toString().toLowerCase();

		for (Game g: games) {
//			String gameGenre = g.getGenre().toLowerCase();

			if (g.getGenre().toLowerCase().equals(genre.toString().toLowerCase())) {
				count++;
			}
		}
		return count;
	}

	// step c vii.
	private void printTopTenSortedGames() {

		Collections.sort(games);

		for (int i = 0; i < 10; i++) {
			System.out.println(games.get(i).toString());
		}
	}

	// step c viii.
	private void exportSortedGames(String filePath) {

		File myFile = new File(filePath);

		Collections.sort(games);

		try (BufferedWriter bW = new BufferedWriter(new FileWriter(myFile))) {

			for (Game g: games) {
				String line = g.getName() + "," + g.getYear() + "," + g.getPublisher();
				bW.write(line + "\n");
			}

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
	}

	// step c ix.
	private Map<Integer, Integer> getCountOfGamesPerYear() {

		Map<Integer, Integer> gamesPerYear = new TreeMap<>(); // use TreeMap instead of HashMap so the key is sorted

		for (Game g: games) {

			Integer count = gamesPerYear.get(g.getYear());
			gamesPerYear.put(g.getYear(), (count == null) ? 1: count+1);
		}

		return gamesPerYear;
	}
}
